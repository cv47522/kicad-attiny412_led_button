# ATtiny412 + LED + Button

KiCad library repository: <https://gitlab.com/cv47522/kicad-library>

## Code

- Blink: <https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_blink>
- Button Blink: <https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_blink_button>
- Echo: <https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_echo>
